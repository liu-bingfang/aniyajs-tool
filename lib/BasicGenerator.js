const Generator = require('yeoman-generator');
const glob = require('glob');
const fs = require('fs');
const chalk = require('chalk');
const { filePathFn } = require('./utils');
const path = require('path');

class BasicGenerator extends Generator {
  constructor(args, customOpt) {
    super(args)
    this.args = args;
    this.customOpt = customOpt;
    this.name = path.basename(args.env.cwd);
  }

  writeFiles({ context, filterFiles }){
    const { language, projectName } = context;

    // 同目录下禁止出现同名文件夹
    if (fs.existsSync(this.destinationPath(projectName))) {
      console.log();
      console.log(
        chalk.red(`> The folder already exists in the current directory.`)
      );
      console.log(
        chalk.yellow('Filename: ') + chalk.cyan(this.destinationPath(projectName))
      );
      process.exit(1)
    }

    console.log();
    glob
      .sync('**/*', {
        cwd: this.templatePath(),
        dot: true,
      })
      .filter(filterFiles)
      .forEach(file => {
        const filePath = this.templatePath(file);

        if (fs.statSync(filePath).isFile()) {
          this.fs.copyTpl(
            filePath,
            this.destinationPath(projectName, filePathFn(file, language)),
            context
          )
        }
      })
  }
}


module.exports = BasicGenerator