process.on("unhandledRejection", (err) => {
  throw err;
});

const inquirer = require("inquirer");
const fs = require("fs-extra");
const path = require("path");
const chalk = require("chalk");
const yeoman = require("yeoman-environment");

const generators = fs
  .readdirSync(path.join(__dirname, "generators"))
  .filter((generatorsPath) => !generatorsPath.startsWith("."))
  .map((generatorsPath) => ({
    name: require(`./generators/${generatorsPath}/meta.json`).description,
    value: generatorsPath,
    short: require(`./generators/${generatorsPath}/meta.json`).title,
  }));

const templates = (answer) => {
  const { whichType } = answer;
  const templateTypes =
    require(`./generators/${whichType}/meta.json`).templateTypes;

  return templateTypes.map((templateType) => ({
    name: `${templateType.name.padEnd(20)} - ${templateType.title}`,
    value: templateType.name,
    short: templateType.name,
  }));
};

const runGenerator = async (templatePath, customOpt) => {
  return new Promise((resolve) => {
    const Generator = require(templatePath);

    const env = yeoman.createEnv([], {
      cwd: process.cwd(),
    });

    const generator = new Generator(
      {
        env,
        resolved: require.resolve(templatePath),
      },
      customOpt
    );

    resolve(generator.run());
  });
};

const run = async () => {
  try {
    const answer = await inquirer.prompt([
      {
        type: "list",
        name: "whichType",
        message: "🎉 请选择你需要的操作",
        choices: generators,
      },
      {
        type: "list",
        name: "templateType",
        message: ({ whichType }) => {
          if (whichType === "scaffolds") {
            console.log();
            return "💖 请选择你需要的模板";
          }

          if (whichType === "blocks") {
            console.log();
            console.log("欢迎使用 Aniyajs模板 🥰");
            console.log();
            console.log();
            return "🧙 Aniyajs Pro";
          }

          if (whichType === "tools") {
            console.log();
            console.log("欢迎使用 Aniyajs工具库 🥰");
            console.log();
            console.log();
            return "Aniyajs Tools";
          }
        },
        choices: templates,
      },
    ]);

    const { whichType, templateType } = answer;
    const templatePath =
      (whichType === "scaffolds" && `./generators/${whichType}`) ||
      (whichType === "tools" && `./generators/${whichType}/${templateType}`) ||
      (whichType === "blocks" && `./generators/${whichType}/${templateType}`);

    await runGenerator(templatePath, { templateType });

    console.log();
    console.log("✨ 生成成功");
  } catch (error) {
    console.log();
    console.log(chalk.red("💥 生成失败"), error);
    process.exit(1);
  }
};

module.exports = run;
