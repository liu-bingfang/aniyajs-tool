const chalk = require("chalk");
const BasicGenerator = require("../../../BasicGenerator");
const fs = require('fs-extra');
const execa = require('execa');
const glob = require('glob');

class Generator extends BasicGenerator {
  constructor(args, _customOpt) {
    super(args)
    this.newAnswer = {}
  }

  async initializing() {
    const prompts = [
      {
        type: 'list',
        name: 'normalizeType',
        message: '🎬 请选择你的使用场景',
        choices: [
          {
            name: 'JavaScript',
            value: 'js',
            short: 'JS',
          },
        ],
      },
    ]

    const answer = await this.prompt(prompts);

    this.newAnswer = answer
  }

  async writing(){
    const packagePath = this.destinationPath('package.json');

    // 检测 package.json 文件是否存在
    if (!fs.existsSync(packagePath)) {
      console.log();
      console.log(
        chalk.red(`> The package.json file is must be exists.`)
      );
      process.exit(1)
    }

    // 写入 package.json 配置
    const packageJson = fs.readJSONSync(packagePath);
    const newPackageJson = {
      ...packageJson,
      scripts: {
        ...(packageJson?.scripts ?? {}),
        prepare: "husky install && husky add .husky/pre-commit 'npx lint-staged'",
        prettier: "prettier -c --write \"**/*\"",
        lint: "eslint ."
      },
      "lint-staged": {
        "**/*": [
          "prettier --write --ignore-unknown",
          "npm run lint"
        ]
      },
    }
    fs.writeJSONSync(
      packagePath,
      newPackageJson,
      {
        spaces: 2,
      }
    )
    // copy相关配置文件
    console.log();
    glob
      .sync('**/*', {
        cwd: this.templatePath('js'),
        dot: true,
      })
      .forEach(file => {
        const filePath = this.templatePath('js', file);

        if (fs.statSync(filePath).isFile()) {
          this.fs.copyTpl(
            filePath,
            this.destinationPath(file),
            this.newAnswer
          )
        }
      })
  }

  async install() {
    const downloadPluginArgs = [
      'install',
      'husky@8.0.0',
      'eslint@8.54.0',
      'lint-staged@15.2.0',
      'prettier@3.1.0',
      '--save-dev',
    ]

    console.log();
    console.log(`Ready to download the relevant plugins...`);
    console.log();

    // 自动执行 npm i husky@8.0.0 eslint@8.54.0 lint-staged@15.2.0 prettier@3.1.0
    await execa(
      'npm', 
      downloadPluginArgs, 
      {
        stdout: process.stdout,
        stderr: process.stderr,
        stdin: process.stdin,
      }
    )
  }

  end() {
    console.log();
    console.log(`If you want to use ${chalk.yellow("`git commit`")} interception, do ${chalk.yellow("`npm run prepare`")}, some more commands include ${chalk.yellow("`prettier`")}、${chalk.yellow("`lint`")}.`);
  }
}

module.exports = Generator;
