/**
 * @type { import("eslint").ESLint.ConfigData }
 */
module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  overrides: [
    {
      env: {
        node: true,
      },
      files: [".eslintrc.{js,cjs}"],
      parserOptions: {
        sourceType: "script",
      },
    },
  ],
  // plugins: ["aniya"],
  parserOptions: {
    ecmaVersion: "latest",
  },
  ignorePatterns: [".eslintignore", "package.json"],
  rules: {
    // 启用双引号
    quotes: ["error", "double"],
    // 启用句末尾分号
    semi: ["error", "always"],
    // var 变量需要在函数顶部
    "vars-on-top": "error",
    // 常量需要使用全大写
    // "aniya/const-upper": "error",
    // 变量 下划线/小驼峰/全小写
    camelcase: [
      "error",
      {
        properties: "always",
        ignoreDestructuring: false,
        ignoreImports: false,
        ignoreGlobals: false,
      },
    ],
    // 对于赋值后永不声明的值必须使用const
    "prefer-const": "off",
    // 可以使用 var
    "no-var": "off",
    // 强制句末尾至少要有一个换行
    "eol-last": ["off", "always"],
    // 未使用的变量
    "no-unused-vars": "off",
    // 尾随逗号
    "comma-dangle": [
      "off",
      {
        arrays: "always",
        objects: "always",
        imports: "always",
        exports: "always",
        functions: "never",
      },
    ],
    // 函数名与括号间需要存在间距，但是匿名函数不需要
    "space-before-function-paren": ["off", "always"],
    // 不禁用行尾处的拖尾空白
    "no-trailing-spaces": "off",
    // 禁止重新分配常量
    "no-const-assign": "error",
    // 任何未声明的变量的引用
    "no-undef": "error",
    // 缩进
    indent: "off",
    // 允许在没有参数的时候调用 Promise.reject()
    "prefer-promise-reject-errors": "off",
    // 允许使用 ==
    eqeqeq: ["off", "always"],
  },
};
