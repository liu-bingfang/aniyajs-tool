const BasicGenerator = require("../../BasicGenerator");
const execa = require('execa');
const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const sylvanas = require('sylvanas');
const { glob } = require('glob');

function globList(patternList, options) {
  let fileList = [];
  patternList.forEach((pattern) => {
    fileList = [...fileList, ...glob.sync(pattern, options)];
  });

  return fileList;
}

class Generator extends BasicGenerator {
  async prompting() {
    const prompts = [
      {
        type: 'list',
        name: 'language',
        message: '🤓 你想用哪种语言TypeScript/JavaScript？',
        default: 'ts',
        choices: [
          {
            name: 'TypeScript',
            value: 'ts',
            short: 'TS',
          },
          {
            name: 'JavaScript',
            value: 'js',
            short: 'JS',
          },
        ],
      },
    ]

    const answer = await this.prompt(prompts);
    console.log();

    this.newAnswer = answer;
  }

  async writing() {
    const { language = 'ts' } = this.newAnswer;

    const gitUrl = `https://gitee.com/liu-bingfang/${this.customOpt.templateType}.git`;
    const gitArgs = ['clone', gitUrl, '--depth=1', '--branch', 'main'];
    const projectName = process.argv[2] || this.name;
    const projectPath = path.resolve();
    const envOptions = {
      cwd: path.resolve(projectName === '.' ? '' : projectName),
    };

    gitArgs.push(projectName);

    if (
      fs.existsSync(projectPath) &&
      fs.statSync(projectPath).isDirectory() &&
      fs.readdirSync(projectPath).length > 0 &&
      projectName === '.'
    ) {
      console.log(
        `\n\n\n🙈 请在空文件夹中使用，或者使用 ${chalk.red('yarn create aiolosjs myapp')}`,
      );
      process.exit(0);
    }

    if (fs.existsSync(envOptions.cwd) && projectName !== '.') {
      console.log(`\n\n\n🐸 ${chalk.red('test' + ' 文件已存在')}`);
      process.exit(0);
    }

    await execa('git', gitArgs, {
      stdout: process.stdout,
      stderr: process.stderr,
      stdin: process.stdin,
    });

    if (fs.existsSync(path.resolve(projectName === '.' ? '' : projectName, '.git'))) {
      fs.rmSync(path.resolve(projectName === '.' ? '' : projectName, '.git'), {
        recursive: true,
      });
    }

    console.log();
    console.log(`🚚 clone success`);

    if (language === 'js') {
      console.log();
      console.log('[Sylvanas] Prepare js environment...');

      const tsFiles = globList(['**/*.tsx', '**/*.ts'], {
        ...envOptions,
        ignore: ['**/*.d.ts', 'node_modules/*'],
      });
      const removeFiles = globList(['**/*.d.ts'], {
        ...envOptions,
      });
      const packageFile = require(path.resolve(envOptions.cwd, 'package.json'));

      sylvanas(tsFiles, {
        ...envOptions,
        action: 'overwrite',
      });

      removeFiles.forEach((filePath) => {
        const allFilePath = path.resolve(envOptions.cwd, filePath);

        fs.rmSync(allFilePath);
      });

      fs.renameSync(
        path.resolve(envOptions.cwd, 'tsconfig.json'),
        path.resolve(envOptions.cwd, 'jsconfig.json'),
      );

      if (packageFile.dependencies) {
        for (const key in packageFile.dependencies) {
          if (/^@types\/.*$/.test(key)) {
            delete packageFile.dependencies[key];
          }
        }
      }

      if (packageFile.devDependencies) {
        for (const key in packageFile.devDependencies) {
          if (/^@types\/.*$/.test(key)) {
            delete packageFile.devDependencies[key];
          }
        }
      }

      fs.rmSync(path.resolve(envOptions.cwd, 'package.json'));
      fs.writeFileSync(path.resolve(envOptions.cwd, 'package.json'), JSON.stringify(packageFile), {
        encoding: 'utf-8',
      });

      console.log();
      console.log('[JS] Clean up...');
    }
  }
}

module.exports = Generator