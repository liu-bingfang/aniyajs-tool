import { queryPost } from '@/services/api';
<% if ( isCreate || isUpdate ) { %>
import { delay } from '@/utils/common';
<% } %>
<% if(language === "ts") { %>
import type { Reducer, Effect } from 'umi';
import type { IGenerato, ITableData } from '@/utils/types';

export interface <%= firstUppercaseProjectName %>DataProps {
  id: number;
  name: string;
  sex: string;
  remark: string;
}

export interface <%= firstUppercaseProjectName %>State {
  tableData: ITableData<<%= firstUppercaseProjectName %>DataProps>;

  modalVisible: boolean;
  confirmLoading: boolean;
  <% if ( isQueryDetail ) { %>
  detailInfo: Partial<<%= firstUppercaseProjectName %>DataProps>;
  <% } %>
}

export interface <%= firstUppercaseProjectName %>ModelType {
  namespace: '<%= lowerCaseProjectName %>';
  state: <%= firstUppercaseProjectName %>State;
  effects: {
    fetch: Effect;
    <% if ( isQueryDetail ) { %>
    fetchDetailInfo: Effect;
    <% } %>
    <% if ( isCreate ) { %>
    create: Effect;
    <% } %>
    <% if ( isUpdate ) { %>
    update: Effect;
    <% } %>
    <% if ( isDelete ) { %>
    remove: Effect;
    <% } %>
  };
  reducers: {
    save: Reducer<<%= firstUppercaseProjectName %>State>;
    clear: Reducer<<%= firstUppercaseProjectName %>State>;
  };
}
<% } %> 
const <%= firstLowercaseProjectName %>Model<% if(language === "ts") { %>: <%= firstUppercaseProjectName %>ModelType<% } %> = {
  namespace: '<%= lowerCaseProjectName %>',
  state: {
    tableData: {
      list: [],
      pagination: {},
    },

    modalVisible: false,
    confirmLoading: false,
    <% if ( isQueryDetail ) { %>
    detailInfo: {},
    <% } %>
  },
  effects: {
    *fetch({ payload }, { call, put })<% if(language === "ts") { %>: IGenerator<% } %>  {
      delete payload.cb;
      const response = yield call(queryPost, '/standardtable/list', payload);

      if (response) {
        const { code, result } = response;
        if (code === 200) {
          yield put({
            type: 'save',
            payload: {
              tableData: result,
            },
          });
        }
      }
    },
    <% if ( isCreate ) { %>
    *create({ payload }, { call, put })<% if(language === "ts") { %>: IGenerator<% } %> {
      const { cb } = payload;
      delete payload.cb;
      yield put({
        type: 'save',
        payload: {
          confirmLoading: true,
        },
      });

      yield call(delay);

      const response = yield call(queryPost, '/standardtable/add', payload);

      yield put({
        type: 'save',
        payload: {
          confirmLoading: false,
        },
      });

      if (response) {
        const { code, result } = response;
        if (code === 200) {
          yield put({
            type: 'save',
            payload: {
              tableData: result,
              modalVisible: false,
            },
          });
          <% if ( isLang) { %>
          cb?.('<%= lowerCaseProjectName %>.global.message.create');
          <% } else { %>
          cb?.('添加成功');
          <% } %>
        }
      }
    },
    <% } %>
    <% if ( isUpdate ) { %>
    *update({ payload }, { call, put })<% if(language === "ts") { %>: IGenerator<% } %> {
      const { cb } = payload;
      delete payload.cb;
      yield put({
        type: 'save',
        payload: {
          confirmLoading: true,
        },
      });

      yield call(delay);

      const response = yield call(queryPost, '/standardtable/update', payload);

      yield put({
        type: 'save',
        payload: {
          confirmLoading: false,
        },
      });

      if (response) {
        const { code, result } = response;
        if (code === 200) {
          yield put({
            type: 'save',
            payload: {
              tableData: result,
              modalVisible: false,
            },
          });
          <% if ( isLang) { %>
          cb?.('<%= lowerCaseProjectName %>.global.message.update');
          <% } else { %>
          cb?.('修改成功');
          <% } %>
        }
      }
    },
    <% } %>
    <% if ( isDelete ) { %>
    *remove({ payload }, { call, put })<% if(language === "ts") { %>: IGenerator<% } %> {
      const { cb } = payload;
      delete payload.cb;
      const response = yield call(queryPost, '/standardtable/delete', payload);
      if (response) {
        const { code, result } = response;
        if (code === 200) {
          yield put({
            type: 'save',
            payload: {
              tableData: result,
            },
          });
          <% if ( isLang) { %>
          cb?.('<%= lowerCaseProjectName %>.global.message.remove');
          <% } else { %>
          cb?.('删除成功');
          <% } %>
        }
      }
    },
    <% } %>
    <% if ( isQueryDetail ) { %>
    *fetchDetailInfo({ payload }, { call, put })<% if(language === "ts") { %>: IGenerator<% } %> {
      const response = yield call(queryPost, '/standardtable/detail', payload);
      if (response) {
        const { code, result } = response;
        if (code === 200) {
          yield put({
            type: 'save',
            payload: {
              detailInfo: result,
            },
          });
        }
      }
    },
    <% } %>
  },
  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    // @ts-ignore
    clear(state) {
      return {
        ...state,
        modalVisible: false,
        confirmLoading: false,
      };
    },
  },
};

export default <%= firstLowercaseProjectName %>Model;
