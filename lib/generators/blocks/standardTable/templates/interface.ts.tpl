import type { DvaLoadingType } from '@/utils/types';
import type { <%= firstUppercaseProjectName %>State } from './model';

export type OperatorKeys = 'fetch' <% if ( isCreate ) { %>| 'create'<% } %> <% if ( isUpdate ) { %>| 'update'<% } %> <% if ( isDelete ) { %>| 'remove'<% } %>;
export type OperatorType = { [K in OperatorKeys]?: string };

export type IRootState = {
  <%= lowerCaseProjectName %>: <%= firstUppercaseProjectName %>State;
  loading: DvaLoadingType;
};
