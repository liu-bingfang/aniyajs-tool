<% if(language === "ts") { %>
import type { ColumnProps } from 'antd/lib/table';
import type { RenderFormItemPropsType } from '@/components/RenderFormItem/renderFormItem';
<% } %>
<% if(isLang && language === "ts") { %>
import type { IntlShape } from 'react-intl';
<% } %>
const formItemPropsLayouts = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 18,
  },
};
<% if ( isCreate || isUpdate ) { %>
const detailFormItemPropsLayouts = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 20,
  },
};
<% } %>
<% if(language === "ts") { %>
export interface PageConfigTypes<T> {
  name: string;
  path: string;
  tableColumns?: ColumnProps<T>[];
  searchFormItems: RenderFormItemPropsType[] | [];
  detailFormItems?: RenderFormItemPropsType[] | [];
}
<% } %>
function pageConfig<% if(language === "ts") { %><T><% } %>(<% if(isLang) { %>intl<% if(language === "ts") { %>: IntlShape<% } %><% } %>)<% if(language === "ts") { %>: PageConfigTypes<T><% } %> {
  return {
    name: '<%= name%>',
    path: '<%= lowerCaseProjectName %>',
    searchFormItems: [
      {
        widget: 'AInput',
        <% if ( isLang) { %>
        label: intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.pageConfig.searchFormItems.name',
        }),
        <% } else { %>
        label:"姓名",
        <% } %>
        name: 'name',
        formItemProps: {
          ...formItemPropsLayouts,
        },
        widgetProps: {},
      },
    ],
    tableColumns: [
      {
        <% if ( isLang) { %>
        title: intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.pageConfig.tableColumns.name',
        }),
        <% } else { %>
        title:"姓名",
        <% } %>
        dataIndex: 'name',
      },
      {
        <% if ( isLang) { %>
        title: intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.pageConfig.tableColumns.sex',
        }),
        <% } else { %>
        title:"性别",
        <% } %>
        dataIndex: 'sex',
      },
    ],
    <% if ( isCreate || isUpdate ) { %>
    detailFormItems: [
      {
        widget: 'AInput',
        name: 'id',
        formItemProps: {
          ...detailFormItemPropsLayouts,
          initialValue: -1,
        },
        colSpan: 0,
      },
      {
        widget: 'AInput',
        name: 'name',
        <% if ( isLang) { %>
        label: intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.pageConfig.detailFormItems.name',
        }),
        <% } else { %>
        label:"姓名",
        <% } %>
        required: true,
        colSpan: 24,
        formItemProps: {
          ...detailFormItemPropsLayouts,
        },
      },
      {
        widget: 'ASelect',
        name: 'sex',
        <% if ( isLang) { %>
        label: intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.pageConfig.detailFormItems.sex',
        }),
        <% } else { %>
        label:"性别",
        <% } %>
        required: true,
        colSpan: 24,
        formItemProps: {
          ...detailFormItemPropsLayouts,
        },
        widgetProps: {
          options: [
            {
              label: '男',
              value: '男',
            },
            {
              label: '女',
              value: '女',
            },
          ],
        },
      },

      {
        widget: 'AInputTextArea',
        name: 'remark',
        <% if ( isLang) { %>
        label: intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.pageConfig.detailFormItems.remark',
        }),
        <% } else { %>
        label:"备注",
        <% } %>
        required: false,
        colSpan: 24,
        formItemProps: {
          ...detailFormItemPropsLayouts,
        },
        widgetProps: {
          autoSize: { minRows: 5, maxRows: 10 },
        },
      },
    ],
    <% } %>
  };
}

export default pageConfig;
