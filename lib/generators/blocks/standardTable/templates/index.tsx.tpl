import React, { useEffect, <% if ( isCreate || isUpdate || isQueryDetail ) { %>useState,<% } %> useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { <% if ( isCreate ) { %>Button,<% } %> Card, <% if ( isCreate || isUpdate ) { %>Modal,<% } %> <% if ( isDelete ) { %>Popconfirm, <% } %> <% if ( isQueryDetail || isDelete || isUpdate ) { %>Row, Col, Tooltip,<% } %> <% if ( isQueryDetail ) { %>Drawer,<% } %> message } from 'antd';
<% if ( isUpdate || isDelete || isCreate || isQueryDetail ) { %>
import { <% if ( isUpdate ) { %>EditOutlined, <% } %><% if ( isDelete ) { %>DeleteOutlined, <% } %><% if ( isCreate ) { %>PlusOutlined, <% } %><% if ( isQueryDetail ) { %> EyeOutlined<% } %> } from '@ant-design/icons';
<% } %>
<% if ( isCreate || isUpdate ) { %>
// @ts-ignore
import clonedeep from 'lodash/cloneDeep';
<% } %>
import { useDispatch, useSelector<% if(isLang) { %>, useIntl<% } %> } from 'umi';
import { useQueryFormParams } from '@/utils/hooks';
import { formaterObjectValue<% if ( isCreate || isUpdate ) { %>, formAddInitValue<% } %> } from '@/utils/common';

import SearchForms from '@/components/SearchForms';
import pageConfig from './pageConfig';
import TableList from '@/components/TableList';
<% if ( isCreate || isUpdate ) { %>
import ModalDetailForm from './ModalDetailForm';
<% } %> 
<% if ( isQueryDetail ) { %>
import DetailInfo from './Detail';
<% } %> 
<% if(language === "ts") { %>
import type { <%= firstUppercaseProjectName %>DataProps } from './model';
import type { IRootState, OperatorKeys<% if ( isCreate || isUpdate ) { %>, OperatorType<% } %> } from './interface';
<% if ( isQueryDetail || isDelete || isUpdate ) { %>
import type { ColumnProps } from 'antd/lib/table';
<% } %>
<% if ( isCreate || isUpdate ) { %>
import type { ModalDetailFormRef } from './ModalDetailForm';
import type { RenderFormItemPropsType } from '@/components/RenderFormItem/renderFormItem';
<% } %>
<% } %> 
<% if ( isCreate || isUpdate ) { %>
const operatorTypeDic<% if(language === "ts") { %>: OperatorType<% } %> = {
  <% if ( isLang ) { %>
  <% if ( isCreate ) { %>
  create: '<%= lowerCaseProjectName %>.Modal.ModalDetailForm.title.create',
  <% } %>
  <% if ( isUpdate ) { %>
  update: '<%= lowerCaseProjectName %>.Modal.ModalDetailForm.title.update',
  <% } %>
  <% } else { %>
  <% if ( isCreate ) { %>
  create: '新建用户',
  <% } %>
  <% if ( isUpdate ) { %>
  update: '修改用户',
  <% } %>
  <% } %>
};
<% } %>
export default ()<% if(language === "ts") { %>: React.ReactNode<% } %> => {
  const dispatch = useDispatch();
  <% if(isLang) { %>
  const intl = useIntl();
  <% } %> 
  const { loading, <%= lowerCaseProjectName %> } = useSelector((state<% if(language === "ts") { %>: IRootState<% } %>) => state);
  const [payload, { setQuery, setPagination<% if ( isCreate ) { %>, setFormAdd<% } %><% if ( isUpdate || isDelete ) { %>, setFormUpdate<% } %> }] = useQueryFormParams();

  const modelReduceType = useRef<% if(language === "ts") { %><OperatorKeys><% } %>('fetch');

  const { searchFormItems = [], tableColumns = []<% if ( isCreate || isUpdate ) { %>, detailFormItems = []<% } %> } = pageConfig<% if(language === "ts") { %><
    <%= firstUppercaseProjectName %>DataProps
  ><% } %>(<% if(isLang) { %>intl<% } %>);
  <% if ( isCreate || isUpdate ) { %>
  const { modalVisible, confirmLoading } = <%= lowerCaseProjectName %>;

  const detailFormRef = useRef<% if(language === "ts") { %><ModalDetailFormRef | null><% } %>(null);

  const [modalTitle, setModalTitle] = useState<% if(language === "ts") { %><string | undefined><% } %>();
  const [modalType, setModalType] = useState<% if(language === "ts") { %><OperatorKeys><% } %>();
  
  const [formItems, setFormItems] = useState<% if(language === "ts") { %><RenderFormItemPropsType[]><% } %>([]);
  <% } %>
  <% if ( isQueryDetail ) { %>
  const [currentItem, setCurrentItem] = useState<% if(language === "ts") { %><<%= firstUppercaseProjectName %>DataProps | {}><% } %>({});

  const [drawerVisible, setDrawerVisible] = useState(false);
  <% } %>
  useEffect(() => {
    dispatch({
      type: `<%= lowerCaseProjectName %>/${modelReduceType.current}`,
      payload: {
        ...payload,
        cb: (text<% if(language === "ts") { %>: string<% } %>) => {
          message.success(
            <% if ( isLang) { %>
            intl.formatMessage({
              id: text,
            }),
            <% } else { %>
            text
            <% } %>
          );
        },
      },
    });
  }, [payload]);
  <% if ( isCreate || isUpdate ) { %>
  const updateFormItems = (record<% if(language === "ts") { %>: <%= firstUppercaseProjectName %>DataProps | {}<% } %>) => {
    const newFormItems = formAddInitValue([...clonedeep(detailFormItems)], record);

    setFormItems([...newFormItems]);
  };

  const changeModalVisible = (flag<% if(language === "ts") { %>: boolean<% } %>) => {
    dispatch({
      type: '<%= lowerCaseProjectName %>/save',
      payload: {
        modalVisible: flag,
      },
    });
  };

  const showModalVisibel = (type<% if(language === "ts") { %>: OperatorKeys<% } %>, record<% if(language === "ts") { %>: <%= firstUppercaseProjectName %>DataProps | {}<% } %>) => {
    updateFormItems(record);
    setModalTitle(
      <% if ( isLang ) { %>
      intl.formatMessage({
        id: operatorTypeDic[type],
      }),
      <% } else { %>
      operatorTypeDic[type]
      <% } %>
    );
    setModalType(type);
    changeModalVisible(true);
    <% if ( isQueryDetail ) { %>
    setCurrentItem(record);
    <% } %>
  };

  const hideModalVisibel = () => {
    changeModalVisible(false);
    <% if ( isQueryDetail ) { %>
    setCurrentItem({});
    <% } %>
  };

  const modalOkHandle = async () => {
    const fieldsValue = await detailFormRef?.current?.form?.validateFields();

    const fields = formaterObjectValue(fieldsValue);
    <% if(isCreate) { %>
    if (modalType === 'create') {
      modelReduceType.current = 'create';
      setFormAdd(fields);
    }<% } %><% if(isUpdate && isCreate) { %>else<% } %> <% if(isUpdate) { %>if (modalType === 'update') {
      modelReduceType.current = 'update';
      setFormUpdate(fields);
    }<% } %>
  };
  <% } %>
  <% if ( isDelete ) { %>
  const deleteTableRowHandle = (id<% if(language === "ts") { %>: number<% } %>) => {
    modelReduceType.current = 'remove';
    setFormUpdate({ id });
  };
  <% } %>
  <% if ( isQueryDetail ) { %>
  const showDrawer = (record<% if(language === "ts") { %>: <%= firstUppercaseProjectName %>DataProps | {}<% } %>) => {
    setDrawerVisible(true);
    setCurrentItem(record);
  };

  const hideDrawer = () => {
    setDrawerVisible(false);
  };
  <% } %>
  <% if ( isQueryDetail || isDelete || isUpdate ) { %>
  const extraTableColumnRender = ()<% if(language === "ts") { %>: ColumnProps<<%= firstUppercaseProjectName %>DataProps>[] | []<% } %> => {
    return [
      {
        <% if ( isLang) { %>
        title: intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.pageConfig.tableColumns.actions.title',
        }),
        <% } else { %>
        title: "操作",
        <% } %>
        width: 120,
        dataIndex: 'actions',
        render: (_<% if(language === "ts") { %>: any<% } %>, record<% if(language === "ts") { %>: <%= firstUppercaseProjectName %>DataProps<% } %>) => {
          return (
            <div>
              <Row>
                <% if ( isQueryDetail ) { %>
                <Col span={8}>
                  <Tooltip
                    <% if ( isLang) { %>
                    title={intl.formatMessage({
                      id: '<%= lowerCaseProjectName %>.pageConfig.tableColumns.actions.render.Tooltip.show',
                    })}
                    <% } else { %>
                    title="查看"
                    <% } %>
                  >
                    <a
                      onClick={() => {
                        showDrawer(record);
                      }}
                    >
                      <EyeOutlined style={{ fontSize: 18, color: 'rgba(0, 0, 0, 0.65)' }} />
                    </a>
                  </Tooltip>
                </Col>
                <% } %>
                <% if ( isUpdate ) { %>
                <Col span={8}>
                  <Tooltip
                    <% if ( isLang) { %>
                    title={intl.formatMessage({
                      id: '<%= lowerCaseProjectName %>.pageConfig.tableColumns.actions.render.Tooltip.update',
                    })}
                    <% } else { %>
                    title="编辑"
                    <% } %>
                  >
                    <a
                      onClick={() => {
                        showModalVisibel('update', record);
                      }}
                    >
                      <EditOutlined style={{ fontSize: 18, color: 'rgba(0, 0, 0, 0.65)' }} />
                    </a>
                  </Tooltip>
                </Col>
                <% } %>
                <% if ( isDelete ) { %>
                <Col span={8}>
                  <Popconfirm
                    <% if ( isLang) { %>
                    title={intl.formatMessage({
                      id: '<%= lowerCaseProjectName %>.pageConfig.tableColumns.actions.render.Popconfirm.delete',
                    })}
                    <% } else { %>
                    title="确定删除吗？"
                    <% } %>
                    placement="topRight"
                    onConfirm={() => {
                      deleteTableRowHandle(record.id);
                    }}
                  >
                    <Tooltip
                      <% if ( isLang) { %>
                      title={intl.formatMessage({
                        id: '<%= lowerCaseProjectName %>.pageConfig.tableColumns.actions.render.Tooltip.delete',
                      })}
                      <% } else { %>
                      title="删除"
                      <% } %>
                    >
                      <a>
                        <DeleteOutlined style={{ fontSize: 18, color: 'rgba(0, 0, 0, 0.65)' }} />
                      </a>
                    </Tooltip>
                  </Popconfirm>
                </Col>
                <% } %>
              </Row>
            </div>
          );
        },
      },
    ];
  };
  <% } %>
  const renderTableList = () => {
    const tableLoading = loading?.models?.<%= lowerCaseProjectName %>;
    const {
      tableData: { list, pagination },
    } = <%= lowerCaseProjectName %>;
    const newTableColumns = [...tableColumns<% if ( isQueryDetail || isDelete || isUpdate ) { %>, ...extraTableColumnRender()<% } %>];

    const onChange = (current<% if(language === "ts") { %>: number<% } %>, pageSize<% if(language === "ts") { %>?: number<% } %>) => {
      modelReduceType.current = 'fetch';
      setPagination({ current, pageSize });
    };

    return (
      <TableList<% if(language === "ts") { %><<%= firstUppercaseProjectName %>DataProps><% } %>
        loading={tableLoading}
        columns={newTableColumns}
        dataSource={list}
        pagination={{ pageSize: 10, onChange, ...pagination }}
      />
    );
  };

  const searchFormRender = () => {
    const onSubmit = (queryValues<% if(language === "ts") { %>: any<% } %>) => {
      modelReduceType.current = 'fetch';
      const query = formaterObjectValue(queryValues);

      setQuery(query);
    };

    const onReset = () => {
      modelReduceType.current = 'fetch';
      setQuery({});
    };

    return <SearchForms onSubmit={onSubmit} onReset={onReset} formItems={searchFormItems} />;
  };

  return (
    <PageContainer title={false}>
      <Card bordered={false}>
        <div className="tableList">
          <div className="tableList-searchform">{searchFormRender()}</div>

          <% if ( isCreate ) { %>
          <div className="tableList-operator">
            <Button
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => {
                showModalVisibel('create', {});
              }}
            >
              <% if ( isLang) { %>
              {intl.formatMessage({
                id: '<%= lowerCaseProjectName %>.global.tableListOperator.Button.create',
              })}
              <% } else { %>
              新建
              <% } %>
            </Button>
          </div>
          <% } %>
          {renderTableList()}
        </div>
      </Card>
      <% if ( isCreate || isUpdate ) { %>
      <Modal
        title={modalTitle}
        destroyOnClose
        visible={modalVisible}
        confirmLoading={confirmLoading}
        onCancel={hideModalVisibel}
        onOk={modalOkHandle}
      >
        <ModalDetailForm ref={detailFormRef} formItems={formItems} />
      </Modal>
      <% } %>
      <% if ( isQueryDetail ) { %>
      <Drawer
        destroyOnClose
        <% if ( isLang ) { %>
        title={intl.formatMessage({
          id: '<%= lowerCaseProjectName %>.Detail.title',
        })}
        <% } else { %>
        title="详情"
        <% } %>
        width="50%"
        placement="right"
        onClose={hideDrawer}
        visible={drawerVisible}
        zIndex={100}
        style={{
          overflow: 'auto',
        }}
      >
        <DetailInfo currentItem={currentItem} />
      </Drawer>
      <% } %>
    </PageContainer>
  );
};
