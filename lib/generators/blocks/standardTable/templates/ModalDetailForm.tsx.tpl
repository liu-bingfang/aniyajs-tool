import React, { forwardRef, memo, useImperativeHandle } from 'react';
import { Form, Col, Row } from 'antd';
import renderFormItem from '@/components/RenderFormItem';
<% if(language === "ts") { %>
import type { FormInstance } from 'antd/es/form/Form';
import type { RenderFormItemPropsType } from '@/components/RenderFormItem/renderFormItem';

export type ModalDetailFormRef = {
  form: FormInstance;
};

export interface ModalDetailFormProps {
  formItems: RenderFormItemPropsType[] | [];
}
<% } %> 
const ModalDetailForm = forwardRef<% if(language === "ts") { %><ModalDetailFormRef, ModalDetailFormProps><% } %>(
  (props<% if(language === "ts") { %>: ModalDetailFormProps<% } %>, ref) => {
    const { formItems = [] } = props;
    const [form] = Form.useForm();

    useImperativeHandle(ref, () => ({
      form,
    }));

    const renderItem = () => {
      return formItems.map((item) => {
        const { colSpan = 8 } = item;

        return (
          <Col span={colSpan} key={item.name}>
            {renderFormItem(item)}
          </Col>
        );
      });
    };

    return (
      <Form form={form}>
        <Row gutter={24}>{renderItem()}</Row>
      </Form>
    );
  },
);

export default memo(ModalDetailForm);
