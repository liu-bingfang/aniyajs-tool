import React, { useEffect } from 'react';
import { useSelector, useDispatch<% if(isLang) { %>, useIntl<% } %> } from 'umi';
import { Card, Descriptions } from 'antd';

<% if(language === "ts") { %>
import type { IRootState } from './interface';
import type { <%= firstUppercaseProjectName %>DataProps } from './model';
<% } %> 

<% if(language === "ts") { %>
export interface DetailProps {
  currentItem: Partial<<%= firstUppercaseProjectName %>DataProps>;
}
<% } %> 

const DetailInfo<% if(language === "ts") { %>: React.FC<DetailProps><% } %>  = ({ currentItem }) => {
  const dispatch = useDispatch();
  <% if(isLang) { %>
  const intl = useIntl();
  <% } %> 
  const { <%= lowerCaseProjectName %>, loading } = useSelector((state<% if(language === "ts") { %>: IRootState<% } %>) => state);
  const { detailInfo = {} } = <%= lowerCaseProjectName %>;
  const dataLoading = loading.effects['<%= lowerCaseProjectName %>/fetchDetailInfo'];

  useEffect(() => {
    const { id } = currentItem;
    dispatch({
      type: '<%= lowerCaseProjectName %>/fetchDetailInfo',
      payload: { id },
    });
  }, []);

  const { name, sex } = detailInfo;

  return (
    <div>
      <Card loading={dataLoading}>
        <Descriptions>
          <Descriptions.Item
            <% if ( isLang) { %>
            label={intl.formatMessage({
              id: '<%= lowerCaseProjectName %>.Detail.Descriptions.name',
            })}
            <% } else { %>
            label="姓名"
            <% } %>
          >
            {name}
          </Descriptions.Item>
          <Descriptions.Item
            <% if ( isLang) { %>
            label={intl.formatMessage({
              id: '<%= lowerCaseProjectName %>.Detail.Descriptions.sex',
            })}
            <% } else { %>
            label="性别"
            <% } %>
          >
            {sex}
          </Descriptions.Item>
        </Descriptions>
      </Card>
    </div>
  );
};

export default DetailInfo;
