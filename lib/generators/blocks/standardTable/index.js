const BasicGenerator = require('../../../BasicGenerator');
const { firstUpperCase, firstLowerCase } = require('../../../utils');

const hasFeature = (features = [], key = '') => {
  return features.findIndex((f) => f === key) !== -1;
};

class Generator extends BasicGenerator {
  async prompting() {
    const prompts = [
      {
        type: 'input',
        name: 'name',
        default: '标准表格',
        message: '🧵 请输入菜单名称-中文',
      },
      {
        type: 'input',
        name: 'projectName',
        default: 'StandardTable',
        message: '🧵 请输入菜单名称-英文',
      },
      {
        type: 'list',
        name: 'language',
        message: '🤓 你想用TypeScript/JavaScript？',
        choices: [
          {
            name: 'TypeScript',
            value: 'ts',
            short: 'TS',
          },
          {
            name: 'JavaScript',
            value: 'js',
            short: 'JS',
          },
        ],
      },
      {
        type: 'list',
        name: 'isLang',
        message: '🌈 是否启用国际化？',
        choices: [
          {
            name: '是',
            value: true,
            short: '是',
          },
          {
            name: '否',
            value: false,
            short: '否',
          },
        ],
      },
      {
        name: 'features',
        message: '🔧 需要开启哪些功能?',
        type: 'checkbox',
        choices: [
          { name: '新增', value: 'create' },
          { name: '编辑', value: 'update' },
          { name: '删除', value: 'delete' },
          { name: '查看', value: 'queryDetail' },
        ],
      },
    ];

    const answer = await this.prompt(prompts);

    const { projectName, features } = answer;

    Object.assign(answer, {
      ...answer,
      isCreate: hasFeature(features, 'create'),
      isUpdate: hasFeature(features, 'update'),
      isDelete: hasFeature(features, 'delete'),
      isQueryDetail: hasFeature(features, 'queryDetail'),
      firstLowercaseProjectName: firstLowerCase(projectName),
      firstUppercaseProjectName: firstUpperCase(projectName),
      lowerCaseProjectName: projectName.toLowerCase(),
    });

    this.newAnswer = answer;
  }

  writing() {
    const { isLang, isCreate, isUpdate, isQueryDetail, language } = this.newAnswer;

    this.writeFiles({
      context: this.newAnswer,
      filterFiles: f => {
        if (!isLang && f.startsWith('locale')) return false;
        if (!isCreate && !isUpdate && f.startsWith('ModalDetailForm')) return false;
        if (!isQueryDetail && f.startsWith('Detail')) return false;
        if (language === 'js' && f.startsWith('interface')) return false;
        
        return true;
      }
    })
  }
}

module.exports = Generator