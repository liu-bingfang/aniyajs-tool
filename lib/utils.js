const firstUpperCase = ([first, ...rest]) => first.toUpperCase() + rest.join('');
const firstLowerCase = ([first = '', ...rest]) => first.toLowerCase() + rest.join('');
const filePathFn = (file, language = 'ts') => {
  let newFile = file.replace(/\.tpl$/, '');

  if (language === 'js' && /\.ts$/.test(newFile)) {
    newFile = newFile.replace(/\.ts$/, '.js');
  }
  if (language === 'js' && /\.tsx$/.test(newFile)) {
    newFile = newFile.replace(/\.tsx$/, '.jsx');
  }

  return newFile;
};

module.exports = {
  firstUpperCase,
  firstLowerCase,
  filePathFn
};
