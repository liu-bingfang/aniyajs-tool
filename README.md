## 介绍

`@aniyajs/tools` 是一款代码生成工具

### 版本

- 稳定版：[![npm package](https://img.shields.io/npm/v/@aniyajs/tools.svg?style=flat-square)](https://www.npmjs.com/package/@aniyajs/tools) [![NPM downloads](http://img.shields.io/npm/dm/@aniyajs/tools.svg?style=flat)](https://npmjs.org/package/@aniyajs/tools)

### 安装

```bash
$ npm i @aniyajs/tools -g
```

### 使用

```bash
$ npm create aniyajs
$ yarn create aniyajs

## 需要使用脚手架时
$ npm create aniyajs [myapp 或 . 或 不填]
$ yarn create aniyajs [myapp 或 . 或 不填]
```
